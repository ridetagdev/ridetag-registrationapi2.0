# Demonstration of using mongodb with NodeJS and Express
Simple Mongo

## Installation
```bash
git clone https://github.com/ratracegrad/nodejs-with-mongodb
npm install
node app.js
```

## Technology Used
1. Node.js
2. MongoDB
3. Express.js
4. Bootstrap

## Automatic livereload for Front End + Back End
1. Run command "gulp"
2. Navigate to localhost:7000

## How to get running on Windows
1. Install MongoDB and set up
2. Open a new terminal window and run "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"
3. Run this command in that terminal "--dbpath /tmp/data"
5. If above two commands don't work, try --> "C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" --dbpath /tmp/data
6. Go to directory that contains the app itself and run "node app.js"
7. Navigate to localhost:8080

## Heroku + MLab deployment
1. Run 'git push heroku master'
2. MLab is used to host our MongoDB instance
