//Internal Controller used by front end associated w/ APi to view the database
const jwt = require('jsonwebtoken');

const generateToken = function(req, res){
  var host = req.get('host'); //works with postman; localhost:8080
  var origin = req.get('origin');
  console.log("AuthenticationController host: ", host);
  console.log("HOST: ", host, "    ORIGIN: ", origin);

  //checks if request is coming from www.ridetag.next
  if(host == "ridetagregistrationapi.herokuapp.com"){
    //generates a token that expires in 5 seconds
    var token = jwt.sign( { host: host }, process.env.SECRET_KEY, {
      expiresIn: 10
    });

    res.jsonp({
      success: true,
      token: token
    });
  }else{
    res.jsonp({
      success: false,
      message: "Invalid token request credentials.",
      host: host,
      origin: origin,
      originFlag: 'correct'
    })
  }
}//end of generateToken

const verifyToken = function(req, res, next){
  var token = req.body.token || req.headers['token'];
  if(token){
    jwt.verify(token, process.env.SECRET_KEY, function(err, decode){
      if(err){
        res.jsonp({
          "success": false,
          "error": "Invalid token."
        });
      } else {
        next();
      }
    })
  }
  else{
    res.jsonp({
      "success": false,
      "error": "Please send a token."
    });
  }
}//end of verifyToken

module.exports = {
  verifyToken : verifyToken,
  generateToken : generateToken

};
