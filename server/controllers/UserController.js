//Controller that will be hit by external API's
const User = require('../models/user');
const dateTime = require('node-datetime');
const nodemailer = require("nodemailer");
const smtpTransport = require('nodemailer-smtp-transport');

const getUsers = function(req, res){
  User.find({}, function(err, users){
    console.log(users);
    if(err){
      res.jsonp({
        sucess: false,
        error: "Could not retrieve users."
      })
    }
    res.jsonp({
      success: true,
      users: users
    })
  });
}//end of getUsers

const createUser = function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "GET, POST", "PUT");

  var newUser = new User({
    first_name: req.body.first_name_input,
    last_name: req.body.last_name_input,
    email:  req.body.email_input,
    phone: req.body.phone_input,
    password: req.body.password_input,
    city: req.body.city_input,
    invite_code: req.body.invite_code_input ? req.body.invite_code_input : 'N/A',
    created_at: dateTime.create().format('Y-m-d H:M:S')
  });

  //ATTEMPTS TO SEND AN EMAIL
  var transporter = nodemailer.createTransport(smtpTransport({
     service: 'Gmail',
     auth: {
         user: 'kevin.dieu@ridetag.net',
         pass: '112233Kd!'
     }
  }));

  var emailText = "Greetings "+ req.body.first_name_input + " " + req.body.last_name_input + ", \\n"
  + "\\n"
  + " You have been registered for a free RideTag account. Please watch your email this summer, for instructions to complete linking all your ride-hailing service accounts. We can't wait to get your feedback once the service is up and running!"
  + "\\n"
  + "Cheers, \\n"
  + "RideTag Team";

  var emailHtml = "<p>Greetings "+ req.body.first_name_input + " " + req.body.last_name_input + ", </p>"
  + "<br/>"
  + "<p> You have been registered for a free RideTag account. Please watch your email this summer, for instructions to complete linking all your ride-hailing service accounts. We can't wait to get your feedback once the service is up and running! </p>"
  + "<br/>"
  + "<p> Cheers, </p>"
  + "<p> RideTag Team </p>";

  // setup e-mail data with unicode symbols
  var mailOptions = {
      from: "RideTag ✔ <kevin.dieu@ridetag.net>", // sender address
      to: req.body.email_input, // list of receivers
      subject: "Thank you for registering with RideTag", // Subject line
      text: emailText, // plaintext body
      html: emailHtml // html body
  }

  // send mail with defined transport object
  transporter.sendMail(mailOptions, function(error, response){
      if(error){
          console.log(error);
          res.jsonp({
            sucess: false,
            error: error
          })
      }else{
          console.log("Message sent: " + response.message);
          newUser.save(function (err) {
            if (err) {
              res.jsonp({
                success: false,
                error: err
              })
            }
            else{
              res.jsonp({
                success: true
              })
            }
          });
      }
      // if you don't want to use this transport object anymore, uncomment following line
      transporter.close(); // shut down the connection pool, no more messages
  });
}//end of createUser

const deleteUser = function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "GET, POST", "PUT");

  console.log(req.body.id);
  User.findByIdAndRemove({ _id : req.body.id }, function(err){
    if(err){
      res.jsonp({
          "success": false,
          "error": err
      });
    }else{
      res.jsonp({
          "successs": true,
          "message": "Successfully deleted user!"
      });
    }
  });
}//end of deleteUser

const updateUser = function(req, res){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "GET, POST", "PUT");
  User.findById(req.body.id_input, function (err, user) {
    if (err){
      res.jsonp({"status": false, "error": err});
    }
    else{
      user.first_name = req.body.first_name_input;
      user.last_name = req.body.last_name_input;
      user.email = req.body.email_input;
      user.phone = req.body.phone_input;
      user.password = req.body.password_input;
      user.city = req.body.city_input;
      user.invite_code = req.body.invite_code_input ? req.body.invite_code_input : 'N/A';
      user.updated_at = dateTime.create().format('Y-m-d H:M:S');
    }
    user.save(function (error, updatedUser) {
      if (error){
         res.jsonp({"success": false, "error": error});
      }else{
        res.jsonp({
            "success": true,
            "message": "Successfully updated user!"
        });
      }
    });
  });
}//end of updateUser

module.exports = {
  getUsers : getUsers,
  createUser : createUser,
  deleteUser : deleteUser,
  updateUser : updateUser
};
