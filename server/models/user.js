const mongoose = require('mongoose');

module.exports = mongoose.model('User', {
  first_name: {
    type: 'String',
    required: true
  },
  last_name:{
    type: 'String',
    required: true
  },
  email: {
    type: 'String',
    required: true
  },
  phone:{
    type: 'String',
    required: true
  },
  password:{
    type: 'String',
    required: true
  },
  city: {
    type: 'String',
    required: true
  },
  invite_code: 'String',
  created_at: {
    type: 'String',
    required: true
  },
  updated_at: 'String'
});
