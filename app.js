// get all required items
const express = require('express');
const engines = require('consolidate');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const assert = require('assert');
const logger = require('morgan');
const path = require('path');
const favicon = require('serve-favicon');
const config = require('./server/config');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
var secureRoutes = express.Router();
process.env.SECRET_KEY = 'ridetag'; //secret key for JWT authentication
config.setConfigs();
const port = process.env.PORT || 8080;

var app = express();

//controllers
var UserController = require('./server/controllers/UserController');
var InternalController = require('./server/controllers/InternalController');
var AuthenticationController = require('./server/controllers/AuthenticationController');

//connects mongoose
mongoose.connect(process.env.MONGOOSE_CONNECT);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// configure our server
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.engine('html', engines.nunjucks);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/secure-api', secureRoutes);


//ROUTES
app.get('/', InternalController.getLogin); //retrieves login page
app.post('/', InternalController.login); //login attempt

app.get('/index', InternalController.getIndex); //retrieves the index page

app.post('/user', InternalController.createUser); //creates a new user
app.delete('/user', InternalController.deleteUser); //deletes user with specific id
app.put('/user', InternalController.updateUser); //updates user

//EXTERNAL API CALLS ARE AUTHENTICATED USING JWT
app.get('/api/authenticate', AuthenticationController.generateToken); //generates a token
secureRoutes.use(AuthenticationController.verifyToken);
secureRoutes.get('/api/user', UserController.getUsers);
secureRoutes.post('/api/user', UserController.createUser);
secureRoutes.delete('/api/user', UserController.deleteUser);
secureRoutes.put('/api/user', UserController.updateUser);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.listen(port, function() {
    console.log('Server listening on port 8080');
});
